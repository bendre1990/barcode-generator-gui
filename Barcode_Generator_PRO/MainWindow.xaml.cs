﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Windows;
using Microsoft.Win32;
using Barcode_Generator;
using System.Text.RegularExpressions;

#pragma warning disable CS0618 // Typ oder Element ist veraltet

namespace Barcode_Generator_PRO
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void generate_Barcode(object sender, RoutedEventArgs e)
        {
            // Get us some Objects ready - a white Bitmap
            int fixed_width = 0;
            int fixed_height = 0;
            BitmapSource bitmap;
            var visual = new DrawingVisual();

            // Draw the Barcode over the Bitmap
            switch (combobox1.SelectedIndex)
            {
                case 0: // CODE 128 B
                    fixed_width = (textbox1.Text.Length * 28) + 120;
                    fixed_height = 140;
                    if (show_text.IsChecked == false)
                    {
                        fixed_height = 110;
                    }
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(0, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(Code128B.Generate(textbox1.Text), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/code128.ttf#Code 128"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case 1: // CODE 39
                    int pixel_ = 30;
                    Console.WriteLine(textbox1.Text.Length);
                    fixed_width = (textbox1.Text.Length * 90) + 180 + 20;
                    fixed_height = 150;
                    if (show_text.IsChecked == false)
                    {
                        fixed_height = 110;
                    }
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(-10, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(Code39.Generate(textbox1.Text), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/Code-39-Logitogo.ttf#Code-39-Logitogo"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case 2: // EAN 13
                    fixed_width = 270;
                    fixed_height = 130;
                    show_text.IsChecked = false; // Disable our text under Barcode because the Font itself brings this functionality (and actually enforces it)
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(0, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(EAN13.Generate(textbox1.Text), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/ean13.ttf#Code EAN13"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case 3: // EAN 8
                    fixed_width = 270;
                    fixed_height = 130;
                    show_text.IsChecked = false; // Disable our text under Barcode because the Font itself brings this functionality (and actually enforces it)
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(0, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(EAN8.Generate(textbox1.Text), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/ean13.ttf#Code EAN13"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;

            }

            // IF "Show Text" is Checked Take the image again, and Draw the entered Text under the Barcode
            if (show_text.IsChecked == true)
            {
                var image = new DrawingImage(visual.Drawing);
                var visual_data = new DrawingVisual();
                using (DrawingContext drawingContext = visual.RenderOpen())
                {
                    drawingContext.DrawImage(image, new Rect(-30, -90, fixed_width - 10, 140));
                    drawingContext.DrawText(new FormattedText(textbox1.Text, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), 34, Brushes.Black), new Point(0, 0));
                }
                var image_data = new DrawingImage(visual.Drawing);
                image1.Source = image_data;
            }
            // Else, just put the image without text into the image widget
            else
            {
                var image_data = new DrawingImage(visual.Drawing);
                image1.Source = image_data;
            }


        }

        private void Button_save_clicked(object sender, RoutedEventArgs e)
        {
            // Render the Source of the ImageWidget to a new bitmap we can encode to a file
            var bitmap = new RenderTargetBitmap((int)image1.Source.Width, (int)image1.Source.Height, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(image1);
                ctx.DrawRectangle(vb, null,
                    new Rect(new Point(0, 0), new Point(image1.Source.Width, image1.Source.Height)));
            }
            bitmap.Render(dv);

            // Encode to PNG
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));

            // Save to File Dialog
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "barcode"; // Default file name
            dlg.DefaultExt = ".png"; // Default file extension
            dlg.Filter = "PNG File (.png)|*.png"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                using (var stream = new FileStream(dlg.FileName, FileMode.Create))
                {
                    encoder.Save(stream);
                }
            }
            image1.Source = bitmap;
        }
    }
}
