﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Barcode_Generator
{
    class helper_class
    {
        public static BitmapSource CreateEmtpyBitmapSource(int width, int height)
        {
            PixelFormat pf = PixelFormats.BlackWhite;
            int rawStride = width * ((pf.BitsPerPixel + 7) / 8);
            var rawImage = new byte[rawStride * height];
            for (int i = 0; i < width * height; i++)
            {
                rawImage[i] = 255;
            }
            var bitmap = BitmapSource.Create(width, height, 96, 96, pf, null, rawImage, rawStride);
            return bitmap;
        }
    }
}
