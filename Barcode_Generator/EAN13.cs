﻿using System;
using System.Collections.Generic;

namespace Barcode_Generator
{
    class EAN13
    {
        static Dictionary<string, string> TableMapping = new Dictionary<string, string> {
            {"0", "AAAAAA" },
            {"1", "AABABB" },
            {"2", "AABBAB" },
            {"3", "AABBBA" },
            {"4", "ABAABB" },
            {"5", "ABBAAB" },
            {"6", "ABBBAA" },
            {"7", "ABABAB" },
            {"8", "ABABBA" },
            {"9", "ABBABA" },
        };

        static Dictionary<string, char> TableA = new Dictionary<string, char>
        {
            {"0", 'A' },
            {"1", 'B' },
            {"2", 'C' },
            {"3", 'D' },
            {"4", 'E' },
            {"5", 'F' },
            {"6", 'G' },
            {"7", 'H' },
            {"8", 'I' },
            {"9", 'J' },
        };     

        static Dictionary<string, char> TableB = new Dictionary<string, char>
        {
            {"0", 'K' },
            {"1", 'L' },
            {"2", 'M' },
            {"3", 'N' },
            {"4", 'O' },
            {"5", 'P' },
            {"6", 'Q' },
            {"7", 'R' },
            {"8", 'S' },
            {"9", 'T' },
        };

        static Dictionary<string, char> TableC = new Dictionary<string, char>
        {
            {"0", 'a' },
            {"1", 'b' },
            {"2", 'c' },
            {"3", 'd' },
            {"4", 'e' },
            {"5", 'f' },
            {"6", 'g' },
            {"7", 'h' },
            {"8", 'i' },
            {"9", 'j' },
        };

        public static char Checksum(string datastring)
        {
            int count = 1;
            int checksum = 0;
            int number = 0;

            foreach (char item in datastring)
            {
                number = Convert.ToInt32(Char.GetNumericValue(item));
                if (count % 2 == 0)
                {
                    checksum = checksum + (number * 3);
                }
                else
                {
                    checksum = checksum + (number * 1);
                }
                count++;
            }
            checksum = ((checksum % 10) - 10) * -1;

            if (checksum == 10)
            {
                checksum = 0;
            }
            return TableC[checksum.ToString()];
        }

        public static string Construct(string datastring)
        {
            // Get some objects for construction
            string tablecombo;
            string startchar = "";
            string firstblock = "";
            char middle = '*';
            string secondblock = "";
            char checksum;
            char endchar = '+';
            string finalstring = "";

            // Set Startchar
            startchar += datastring[0];

            // Determine the Combination of Tables to use
            tablecombo = TableMapping[startchar];

            // Generate char 2 to 7
            int pos = 1;
            foreach (char item in tablecombo)
            {
                if (item == 'A')
                {
                    firstblock += TableA[datastring[pos].ToString()];
                }
                else
                {
                    firstblock += TableB[datastring[pos].ToString()];
                }
                pos++;
                
            }

            // Generate char 8 to 12
            pos = 7;
            int val;
            foreach (char item in datastring)
            {
                if (pos == 12)
                {
                    break;
                }
                secondblock += TableC[datastring[pos].ToString()];
                pos++;
            }

            // Get the Checksum
            checksum = Checksum(datastring);

            // Construct final string
            finalstring += startchar;
            finalstring += firstblock;
            finalstring += middle;
            finalstring += secondblock;
            finalstring += checksum;
            finalstring += endchar;

            Console.WriteLine(finalstring);
            return finalstring;
        }

        public static string Generate(string datastring)
        {
            if (datastring.Length == 12)
            {
                return Construct(datastring);
            }
            else
            {
                return "";
            }
        }


    }
}
