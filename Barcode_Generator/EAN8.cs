﻿using System;
using System.Collections.Generic;

namespace Barcode_Generator
{
    class EAN8
    {
        static Dictionary<string, string> TableMapping = new Dictionary<string, string> {
            {"0", "AAA" },
            {"1", "AAA" },
            {"2", "AAA" },
            {"3", "AAA" },
            {"4", "AAA" },
            {"5", "AAA" },
            {"6", "AAA" },
            {"7", "AAA" },
            {"8", "AAA" },
            {"9", "AAA" },
        };

        static Dictionary<string, char> TableA = new Dictionary<string, char>
        {
            {"0", 'A' },
            {"1", 'B' },
            {"2", 'C' },
            {"3", 'D' },
            {"4", 'E' },
            {"5", 'F' },
            {"6", 'G' },
            {"7", 'H' },
            {"8", 'I' },
            {"9", 'J' },
        };

        static Dictionary<string, char> TableB = new Dictionary<string, char>
        {
            {"0", 'K' },
            {"1", 'L' },
            {"2", 'M' },
            {"3", 'N' },
            {"4", 'O' },
            {"5", 'P' },
            {"6", 'Q' },
            {"7", 'R' },
            {"8", 'S' },
            {"9", 'T' },
        };

        static Dictionary<string, char> TableC = new Dictionary<string, char>
        {
            {"0", 'a' },
            {"1", 'b' },
            {"2", 'c' },
            {"3", 'd' },
            {"4", 'e' },
            {"5", 'f' },
            {"6", 'g' },
            {"7", 'h' },
            {"8", 'i' },
            {"9", 'j' },
        };

        public static char Checksum(string datastring)
        {
            int count = 1;
            int checksum = 0;
            int number = 0;

            foreach (char item in datastring)
            {
                number = Convert.ToInt32(Char.GetNumericValue(item));
                if (count % 2 == 0)
                {
                    checksum = checksum + (number * 1);
                }
                else
                {
                    checksum = checksum + (number * 3);
                }
                count++;
            }
            checksum = ((checksum % 10) - 10) * -1;
            if (checksum == 10)
            {
                checksum = 0;
            }
            return TableC[checksum.ToString()];
        }

        public static string Construct(string datastring)
        {
            // Get some objects for construction
            string tablecombo;
            string startchar = "";
            string firstblock = "";
            char middle = '*';
            string secondblock = "";
            char checksum;
            char endchar = '+';
            string finalstring = "";

            // Set Startchar
            startchar += TableA[datastring[0].ToString()];

            // Determine the Combination of Tables to use
            tablecombo = "AAA";

            string start_code = ":";

            // Generate char 2 to 4
            int pos = 1;
            foreach (char item in tablecombo)
            {
                if (item == 'A')
                {
                    firstblock += TableA[datastring[pos].ToString()];
                }
                else
                {
                    firstblock += TableB[datastring[pos].ToString()];
                }
                pos++;

            }

            // Generate char 5 to 8
            pos = 4;
            int val;
            foreach (char item in datastring)
            {
                if (pos == 7)
                {
                    break;
                }
                secondblock += TableC[datastring[pos].ToString()];
                pos++;
            }

            // Get the Checksum
            checksum = Checksum(datastring);

            // Construct final string
            finalstring += start_code;
            finalstring += startchar;
            finalstring += firstblock;
            finalstring += middle;
            finalstring += secondblock;
            finalstring += checksum;
            finalstring += endchar;

            Console.WriteLine(finalstring);
            return finalstring;
        }

        public static string Generate(string datastring)
        {
            if (datastring.Length == 7)
            {
                return Construct(datastring);
            }
            else
            {
                return "";
            }
        }


    }
}
