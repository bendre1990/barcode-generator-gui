﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcode_Generator
{
    class Code128B
    {
        public static string Generate(string datastring)
        {
            // Declare constant start/stop characters
            const string startchar = "Ì";
            const string stopchar = "Î";

            // mapping charweights (see https://github.com/Holger-Will/code-128-font for more information)
            var charweights = new Dictionary<char, int>();

            // Add common characters
            for (int i = 0; i < 95; i++)
            {
                charweights.Add((char)(i + 32), i);
            }

            //Add special characters
            charweights.Add((char)0195, 95);
            charweights.Add((char)0196, 96);
            charweights.Add((char)0197, 97);
            charweights.Add((char)0198, 98);
            charweights.Add((char)0199, 99);
            charweights.Add((char)0200, 100);
            charweights.Add((char)0201, 101);
            charweights.Add((char)0202, 102);

            // Calculate Checksum
            int count = 1;
            int weight = 0;
            int checksum = 104;
            foreach (var item in datastring)
            {
                try
                {
                    weight = charweights[item] * count;
                    checksum = checksum + weight;
                    count++;
                }
                catch (KeyNotFoundException)
                {

                    return "";
                }

            }

            // Modulo 103 to determine charweigt value
            checksum = checksum % 103;
            char checkchar = charweights.FirstOrDefault(x => x.Value == checksum).Key;
            // Finally return the raw string the font uses to generate a valid barcode
            return startchar + datastring + checkchar + stopchar;
        }
    }
}
