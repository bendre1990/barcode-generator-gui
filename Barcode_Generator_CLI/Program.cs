﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Windows;
using Microsoft.Win32;
using Barcode_Generator;

#pragma warning disable CS0618 // Typ oder Element ist veraltet

namespace Barcode_Generator_CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            // check arguments given
            // if not expected amount, inform user and terminate
            if (args.Length != 3)
            {
                Console.WriteLine(String.Format("You gave me {0} arguments. Please give me three arguments.", args.Length.ToString()));
                Environment.Exit(1);
            }

            if (args[0].Any(char.IsLower))
            {
                Console.WriteLine("Use UPPERCASE only for code identification");
                Environment.Exit(1);
            }

            // string s = System.IO.Packaging.PackUriHelper.UriSchemePack;
            if (!UriParser.IsKnownScheme("pack"))
            {
                new Application();
            }
            // Get us some Objects ready - a white Bitmap
            int fixed_width = 100;
            int fixed_height = 100;
            BitmapSource bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
            var visual = new DrawingVisual();

            string barcode = args[0];
            string datastring = args[2];
            bool show_text = true;

            if (args[1] == "false")
            {
                show_text = false;
            }

            // Draw the Barcode over the Bitmap
            switch (barcode)
            {
                case "code128b": // CODE 128 B
                    fixed_width = (datastring.Length * 28) + 120;
                    fixed_height = 140;
                    if (show_text == false)
                    {
                        fixed_height = 110;
                    }
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(0, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(Code128B.Generate(datastring), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/code128.ttf#Code 128"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case "code39": // CODE 39
                    fixed_width = (datastring.Length * 90) + 180 + 20;
                    fixed_height = 150;
                    if (show_text == false)
                    {
                        fixed_height = 110;
                    }
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(-10, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(Code39.Generate(datastring), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/Code-39-Logitogo.ttf#Code-39-Logitogo"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case "EAN13": // EAN 13
                    fixed_width = 270;
                    fixed_height = 130;
                    show_text = false; // Disable our text under Barcode because the Font itself brings this functionality (and actually enforces it)
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(0, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(EAN13.Generate(datastring), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/ean13.ttf#Code EAN13"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
                case "EAN8": // EAN 8
                    fixed_width = 175;
                    fixed_height = 130;
                    show_text = false; // Disable our text under Barcode because the Font itself brings this functionality (and actually enforces it)
                    using (DrawingContext drawingContext = visual.RenderOpen())
                    {
                        bitmap = helper_class.CreateEmtpyBitmapSource(fixed_width, fixed_height);
                        drawingContext.DrawImage(bitmap, new Rect(-5, 0, fixed_width, fixed_height));
                        drawingContext.DrawText(new FormattedText(EAN8.Generate(datastring), CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/fonts/ean13.ttf#Code EAN13"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 100, Brushes.Black), new Point(0, 0));
                    }
                    break;
            }

            DrawingImage imagedata;
            // IF "Show Text" is Checked Take the image again, and Draw the entered Text under the Barcode
            if (show_text == true)
            {
                var image = new DrawingImage(visual.Drawing);
                var visual_data = new DrawingVisual();
                using (DrawingContext drawingContext = visual.RenderOpen())
                {
                    drawingContext.DrawImage(image, new Rect(-30, -90, fixed_width - 10, 140));
                    drawingContext.DrawText(new FormattedText(datastring, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface("Segoe UI"), 34, Brushes.Black), new Point(0, 0));
                }
                imagedata = new DrawingImage(visual.Drawing);
            }
            // Else, just put the image without text into the image widget
            else
            {
                imagedata = new DrawingImage(visual.Drawing);
            }


            // Render the Source of the ImageWidget to a new bitmap we can encode to a file
            var bitmap1 = new RenderTargetBitmap((int)imagedata.Width, (int)imagedata.Height, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(visual);
                ctx.DrawRectangle(vb, null,
                    new Rect(new Point(0, 0), new Point(imagedata.Width, imagedata.Height)));
            }
            bitmap1.Render(dv);


            // Encode to PNG
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap1));

            // Save document
            using (var stream = new FileStream(datastring + ".png", FileMode.Create))
            {
                encoder.Save(stream);
            }
        }
    }
}
